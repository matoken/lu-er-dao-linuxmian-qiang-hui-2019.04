---
title: vmdb2 でカスタムOSイメージ作成
description: 
url:
page_number: true
footer: @matoken
marp: true

---
# vmdb2 でカスタムOSイメージ作成


---
## メルカリで Raspberry Pi 3 model B 入手

![](images/rpi3_brack.jpg)

※現在は売り切れ

---
## 2019-04-15時点では白ケースが売られている

![](images/rpi3_white.jpg)


---
## Raspberry Pi 3 model B

* SoC が64bit!
* 公式の Raspbian は armhf(32bit)
* せっかくなら 64bit を使いたい

---

## Debian

* arm64 がある
* 岩松さんの資料を参考に自分でイメージが作れるはず
  * [東京エリアDebian勉強会](https://tokyodebian-team.pages.debian.net/)
    * [Raspberry Pi 2 Model B に Debian Jessie / armhf をインストールする – 第125回 2015年3月度](https://tokyodebian-team.pages.debian.net/pdf2015/debianmeetingresume201503-presentation-iwamatsu.pdf)
    * [Raspberry Pi3 / arm64 – Debian/Ubuntu ミートアップ in 札幌](https://tokyodebian-team.pages.debian.net/pdf2016/debianmeetingresume201606-rpi3-presentation.pdf)

---
## Debian Wikiにイメージがあった

* 自分で作らなくてもDebian Wiki の RaspberryPi3 の項目に非公式イメージが置いてあった
  * [RaspberryPi3 – Debian Wiki](https://wiki.debian.org/RaspberryPi3)
    * [Debian/raspi3-image-spec: contains the files to build the https://wiki.debian.org/RaspberryPi3 image](https://github.com/Debian/raspi3-image-spec/branches)

---
## とりあえず入手して USBメモリに書き込み

```
$ wget -c https://people.debian.org/~gwolf/raspberrypi3/20190206/20190206-raspberry-pi-3-buster-PREVIEW.img.xz https://people.debian.org/~gwolf/raspberrypi3/20190206/20190206-raspberry-pi-3-buster-PREVIEW.img.xz.sha256
$ sha256sum -c ./20190206-raspberry-pi-3-buster-PREVIEW.img.xz.sha256
20190206-raspberry-pi-3-buster-PREVIEW.img.xz: 完了
$ xzcat 20190206-raspberry-pi-3-buster-PREVIEW.img.xz | pv | sudo dd of=/dev/sdb bs=1M oflag=dsync
```

---
## Raspberry Pi のUSBメモリから起動できるようOTP(One Time Programmable)を設定

* Raspberry Pi 3+ シリーズは標準でUSB起動可能なのでこの設定は不要なはず(未入手未検証)
* Raspbian 2017-04-10 以降のイメージで起動して，`vcgencmd` コマンドで該当部分を書き換える
  * [How to boot from a USB mass storage device on a Raspberry Pi – Raspberry Pi Documentation](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/msd.md)

**注意:OTPの設定は1度しか出来ないし，元に戻すことも出来ない(はず)**

---
## OTP書き換え

* 既定値
```
pi@raspberrypi:~$ vcgencmd otp_dump | grep ^17:
17:1020000a
```

* config.txt に `program_usb_boot_mode=1` を書いて再起動
```
$ echo program_usb_boot_mode=1 | sudo tee -a /boot/config.txt
program_usb_boot_mode=1
pi@raspberrypi:~$ sudo reboot
```

* 書き換えを確認
```
pi@raspberrypi:~$ vcgencmd otp_dump | grep ^17:
17:3020000a
```

---

## 書き換えた内容

* [Raspberry Pi Documentation](https://www.raspberrypi.org/documentation/hardware/raspberrypi/otpbits.md) より
```
17 – bootmode register

Bit 1: sets the oscillator frequency to 19.2MHz
Bit 3: enables pull ups on the SDIO pins
Bit 19: enables GPIO bootmode
Bit 20: sets the bank to check for GPIO bootmode
Bit 21: enables booting from SD card
Bit 22: sets the bank to boot from
Bit 28: enables USB device booting
Bit 29: enables USB host booting (ethernet and mass storage)
```
* 設定前が，0x1020000a -> 0b010000001000000000000000001010 で  
  設定後が，0x3020000a -> 0b110000001000000000000000001010  
  Bit 29 が 0 から 1 になってネットワーク起動が可能に

---
## USBメモリからの起動

```
root@rpi3:~# uname -a
Linux rpi3 4.19.0-2-arm64 #1 SMP Debian 4.19.16-1 (2019-01-17) aarch64 GNU/Linux
```

* arm64 :smile:

---
## ところでこのイメージはどうやって作られているのか?

* Debian Wiki に GitHub へのリンクが
* [GitHub - Debian/raspi3-image-spec: contains the files to build the https://wiki.debian.org/RaspberryPi3 image](https://github.com/Debian/raspi3-image-spec)

---
## vmdb2 というツールを利用しているらしい

* vmdb2 = qemu + debootstrap + ansible + α(ディスクイメージ操作関連)
* vmdebootstrap の後継
* 作成したイメージは仮想マシンにも実機にも利用できる
* 別アーキテクチャのイメージも作れる
```
$ dpkg-query -W -f='${Description}\n' vmdb2
creator of disk images with Debian installed
 vmdb2 will be a successor of vmdebootstrap. It will create disk images
 for virtual machines and real hardware, with partitioning, and a boot
 loader, and a Debian installation.
```
* [Debian image builder](https://vmdb2.liw.fi/)

---
## 環境構築

必要なパッケージ導入
```
$ sudo apt install kpartx parted qemu-utils qemu-user-static python3-cliapp \
 python3-jinja2 python3-yaml
```

raspi3-image-spec を clone(vmdb2も)
```
$ git clone --recursive https://github.com/Debian/raspi3-image-spec
$ cd raspi3-image-spec
```
---
## イメージを作成してみる

```
$ time sudo env -i LC_CTYPE=C.UTF-8 PATH="/usr/sbin:/sbin:$PATH" \
  ./vmdb2/vmdb2 --rootfs-tarball=./raspi3.tar.gz \
  --output ./raspi3.img ./raspi3.yaml \
  --log ./raspi3.log --verbose
```
* 30〜40分ほどで出来上がり(回線によって変わるはず)
* `raspi3.img` を使うと普通に起動する :smile:

---
## Debian Buster 以降なら vmdb2 pkg があるのでそっちのほうがいいかも

```
$ sudo apt install kpartx parted qemu-utils qemu-user-static python3-cliapp \
 python3-jinja2 python3-yaml vmdb2
$ git clone https://github.com/Debian/raspi3-image-spec
$ cd raspi3-image-spec
$ time sudo env -i LC_CTYPE=C.UTF-8 PATH="/usr/sbin:/sbin:$PATH" \
  vmdb2 --rootfs-tarball=./raspi3.tar.gz \
  --output ./raspi3.img ./raspi3.yaml \
  --log ./raspi3.log --verbose
```

---
## カスタマイズ

* `raspi3.yaml` を編集してカスタマイズできる
* 中を見ればだいたいわかると思うけど ansible を少しかじっておくとわかりやすい
* vmdb2 固有の部分は [Debian image builder](https://vmdb2.liw.fi/) に載っている

---
## ansible?

* [マンガでわかるRed Hat Ansible Automation](http://redhat.lookbookhq.com/jp_ansible_comic)
* [linklight/README.ja.md at master · network-automation/linklight](https://github.com/network-automation/linklight/blob/master/exercises/ansible_engine/README.ja.md)
* [Ansible ユーザー会 \- connpass](https://ansible-users.connpass.com/) ※リモート枠があるので遠隔からも参加可能

